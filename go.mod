module salsa.debian.org/mdosch/go-sendxmpp

go 1.17

require (
	github.com/gabriel-vasile/mimetype v1.4.0
	github.com/mattn/go-xmpp v0.0.0-20220319135856-e773596ea0b0
	github.com/pborman/getopt/v2 v2.1.0
	salsa.debian.org/mdosch/xmppsrv v0.1.1
)

require golang.org/x/net v0.0.0-20220225172249-27dd8689420f // indirect
