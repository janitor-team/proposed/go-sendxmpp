# Changelog

## [v0.3.0] 2022-03-21
### Added
- Added support for joining password protected MUCs.

### Changed
- Removed invalid code points from input.
- Fixed crash when reading a config with wrong syntax.
- Fixed crash when a non-existing or non-readable config was supplied by `-f`.
- Changed config file location from `~/.config/go-sendxmpp/sendxmpprc` to `~/.config/go-sendxmpp/config`.
- Fixed blocking of go-sendxmpp if an IQ reply of type "error" is received (via go-xmpp v0.0.0-20220319135856-e773596ea0b0).

## [v0.2.0] 2022-02-12
### Added
- Added listening function.
- Added flag to configure connection timeout.
- Added flag to configure minimum TLS version.
- Added flag to show version.

### Removed
- Removed deprecated option `-x`.

## [v0.1.3] 2022-01-29
### Changed
- Rename files to use a limited character set (alpha numerical and some extra characters) file name before uploading. Workaround for https://github.com/mattn/go-xmpp/issues/132

## [v0.1.2] 2021-11-18
### Changed
- Use xml.Marshal to safely build HTTP Upload request.
- Use salsa.debian.org/mdosch/xmppsrv for SRV lookups.

## [v0.1.1] 2021-09-12
### Changed
- Xml-escape file name in http-upload.
- Xml-escape mimetype in http-upload.

## [v0.1.0] 2021-09-11
### Added
- Initial release
